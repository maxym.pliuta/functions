const getSum = (str1, str2) => {
  
  if(typeof str1 != 'string' || typeof str2 != 'string'){
    return false;
  }

  let numb1;
  let numb2;

  if(str1.length == 0){
    numb1 = 0
  }else{
    numb1 = parseInt(str1);
  }
  if(str2.length == 0){
    numb2 = 0
  }else{
    numb2 = parseInt(str2);
  }

  if( isNaN(numb1) || isNaN(numb2)){
    return false;
  }



  return (numb1 + numb2) + '';
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  
  let posts = 0;
  let comments = 0;

  for(post of listOfPosts){
    if(post.author == authorName){
      posts++;
    }
    if('comments' in post){
      for(comment of post.comments){
        if(comment.author == authorName){
          comments++;
        }
      }
    }
    
  }

  return 'Post:' + posts + ',comments:' + comments;
}

const tickets=(people)=> {
  
  let _25 = 0;
  let _50 = 0;
  let isOk = true;


  people.forEach(element => {
    if(element == 25){
      _25++;
    }
    else if(element == 50){
      if(_25 > 0){
        _25--;
        _50++;
      }
      else{
        isOk = false;
      }
    }
    else if(element == 100){
      if(_50 > 0){
        if(_25 > 0){
          _50--;
          _25--;
        }
        else{
          isOk = false;
        }
      }
      else if(_25 >= 3){
        _25 -= 3;
      }
      else{
        isOk = false;
      }
    }
  });

  if(isOk){
    return 'YES';
  }
  else{
    return 'NO'
  }
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
